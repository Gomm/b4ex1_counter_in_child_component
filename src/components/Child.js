import React, {useState} from 'react';


function Child (props){

    let {counter} = props


    return <div> 
            <h1>Odd Number Counter:</h1>
                <p>{counter %2 == 0 && counter !== 0 ? counter-1 : counter}</p>
                <button onClick = {props.buttonClicked}>Click me</button>
          </div>


}






export default Child