import React, {useState} from 'react';

import './App.css';
import Child from './components/Child'

function App() {

  const [count, setCount] = useState (0);

  
  const buttonClicked = ()=> {

    let tempCount = count
    tempCount += 1
    setCount(tempCount)
  }



  return (
    <div className="App">
      <header className="App-header">

        <div className = 'counterBox'>

          <Child counter = {count} buttonClicked = {buttonClicked}/>

        </div>
      
      </header>
    </div>
  );
}

export default App;
